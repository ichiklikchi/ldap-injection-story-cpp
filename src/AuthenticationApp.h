#pragma once
#include <string>
#include "LdapClient.h"

namespace ldap_inj {

    class AuthenticationApp {
        public:
            AuthenticationApp() = default;
            /*
             * \brief Run application 
             */
            void run();
        private:
            /*
             * \brief checks if login exists and take him from console
             */
            bool existsLogin(const std::string& login);
            /*
             * \brief checks if password exists and take him from console
             */
            bool existsPassword(const std::string& login, const std::string& password);
        private:
            LdapClient m_ldap;
    };

} // namespace ldap