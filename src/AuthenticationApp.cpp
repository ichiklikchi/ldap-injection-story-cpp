#include <iostream>
#include "AuthenticationApp.h"

namespace ldap_inj {
    
    void AuthenticationApp::run() {
        std::string login, password;

        std::cout << "Enter login: ";
        std::cin >> login;

        if (not m_ldap.connect("192.168.8.207", 389)) {
            std::cout << "Storage is inaccessible." << std::endl;
            return;
        }

        if (not existsLogin(login)) {
            std::cout << "No login is found, " << login; 
        } else {

            std::cout << "Ok, " << login << ", enter your password: ";
            std::cin >> password;
            
            std::cout << "Password is" << (existsPassword(login, password)? "": "n't") << " correct!";
        }
        std::cout << "\nGoodbye!" << std::endl;
    }
    
    bool AuthenticationApp::existsLogin(const std::string& login) {
        return m_ldap.hasUser(login);
    }
 
    bool AuthenticationApp::existsPassword(const std::string& login, const std::string& password) {
        return m_ldap.hasUser(login, password);
    }

} // namespace ldap